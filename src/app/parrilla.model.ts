export class ParrillaModel {
    horas: string;
    bloque: string;
    
    constructor(horas: string, bloque: string) {
        this.horas = horas;
        this.bloque = bloque;
    }

}