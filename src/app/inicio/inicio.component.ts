import { Component, OnInit } from '@angular/core';
import { CommonService } from '../utils/common.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  constructor(private service: CommonService) { }

  avisos = 'Cargando...';
  directo = 'Buscando...';
  enlace = '';
  paypal = '';

  ngOnInit(){
    this.loadInfo();

    setInterval(() => {
      this.loadInfo();
    }, 50000);
  }
  

  loadInfo() {
    this.service.obtener('info').toPromise().then(response => {
      const aviso = 'aviso';
      const directo = 'directo';
      const paypal = 'paypal';
      this.avisos = response[aviso];
      this.directo = response[directo];
      this.paypal = response[paypal];
    });
  }
}
