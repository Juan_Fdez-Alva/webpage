import { NgModule } from '@angular/core';
import { Routes, RouterModule, provideRoutes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { LoginpgComponent } from './loginpg/loginpg.component';
import { PruebasComponent } from './pruebas/pruebas.component';
import { ParrillaComponent } from './parrilla/parrilla.component';
import { RegistroComponent } from './registro/registro.component';


const routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: '', component: InicioComponent },
  { path: 'login', component: LoginpgComponent },
  { path: 'pruebas', component: PruebasComponent },
  { path: 'parrilla', component: ParrillaComponent },
  { path: 'registro', component: RegistroComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
