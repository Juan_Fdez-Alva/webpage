import { Component, OnInit } from '@angular/core';
import { CommonService } from '../utils/common.service';

@Component({
  selector: 'app-pruebas',
  templateUrl: './pruebas.component.html',
  styleUrls: ['./pruebas.component.scss']
})
export class PruebasComponent implements OnInit {

  constructor(private service: CommonService) { }
  parrilla = [];

  ngOnInit() {
    this.parrilla = [];
    this.updateDatas();
  }

  updateDatas() {
    this.parrilla = [];
    this.loadParrilla();
  }

  loadParrilla() {
    this.service.obtener('random').toPromise().then((data: Array<object>) => {
      data.forEach(elem => {
        this.parrilla.push(elem);
      });
    })
  }

}
