import { Component, OnInit } from '@angular/core';
import { CommonService } from './utils/common.service';
// import { MenuModule } from 'primeng/menu';
import { MenuItem } from 'primeng/api';
import { SelectableRowDblClick } from 'primeng/table';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private service: CommonService, private messageService: MessageService) { }

  title = 'Cargando...';
  val = 50;
  buttonType = 'pi-play';
  menudi = false;
  fondotexto = 'Dia';
  tema = false;
  selected: sour;
  source = 'http://freeuk29.listen2myradio.com:39560/;stream.nsv&amp;type=mp3';
  recursos = [
    { name: 'mctoledo', url: 'http://freeuk29.listen2myradio.com:39560/;stream.nsv&amp;type=mp3'},
    // { name: 'lunaroja', url: 'http://freeuk2.listen2myradio.com:14851/;stream.nsv&amp;type=mp3'},
    { name: 'Dance', url: 'https://21283.live.streamtheworld.com/LOS40_DANCEAAC.aac?csegid=12000&dist=los40-widget-live_streaming_play&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=2c3ba3ba-e5a0-49d1-9654-816d0e9b7b38'},
    { name: 'Clasic', url: 'https://23603.live.streamtheworld.com/LOS40_CLASSICAAC.aac?csegid=12000&dist=los40-widget-live_streaming_play&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=47697dd9-a423-466c-d70d-20447f7ee622' }
  ];


  items: MenuItem[];

  switchMusic() {
    if (this.buttonType === 'pi-pause') {
      this.buttonType = 'pi-play';
      const pause = 'pause';
      document.getElementById('radio')[pause]();
    } else {
      const play = 'play';
      document.getElementById('radio')[play]();
      this.buttonType = 'pi-pause'
    }
  }

  ngOnInit() {
    if (localStorage.getItem('volume')) {
      this.val = JSON.parse(localStorage.getItem('volume'));
    }

    if (localStorage.getItem('radio')) {
      this.source = JSON.parse(localStorage.getItem('radio'));
    }

    if (localStorage.getItem('tema')&&localStorage.getItem('fondotexto')) {
      this.tema = JSON.parse(localStorage.getItem('tema'));
      this.fondotexto = JSON.parse(localStorage.getItem('fondotexto'));
    }

    this.updateVolume();
    this.loadCurrentSong();

    if (window.screen.width <= 800) {
      this.menudi = true;
    } else {
      this.menudi = false;
    }

    this.items = [{
      label: 'Menu',
      items: [
        { label: 'Inicio', icon: 'pi pi-home', routerLink: ['/'] },
        { label: 'Login', icon: 'pi pi-sign-in', routerLink: ['/login'] },
        { label: 'Parrilla', icon: 'pi pi-list', routerLink: ['/parrilla'] },
        { label: 'Pruebas', icon: 'pi pi-file', routerLink: ['/pruebas'] },
      ]
    }];

    setInterval(() => {
      this.loadCurrentSong();
    }, 5000);
    
    setInterval(() => {
      if (window.screen.width <= 800) {
        this.menudi = true;
      } else {
        this.menudi = false;
      }
    }, 1000);
  }
  radio() {
    this.source = this.selected.url;
    localStorage.setItem('radio', JSON.stringify(this.source));
    this.messageService.add({ severity: 'info', detail: 'Acuerdate de recargar la pagina para que funcione el cambio' });
    // window.location.assign('/');
  }

  loadCurrentSong() {
    this.service.obtener('cancion').toPromise().then(response => {
      const song = 'song';
      this.title = response[song];
    });
  }
  updateVolume() {
    const volume = 'volume';
    document.getElementById('radio')[volume] = this.val / 100;
    localStorage.setItem('volume', JSON.stringify(this.val));
  }

  temacolor() {
    if (this.tema) {
      this.fondotexto = 'Noche';
      localStorage.setItem('tema', JSON.stringify(this.tema));
      localStorage.setItem('fondotexto', JSON.stringify(this.fondotexto));
    } else {
      this.fondotexto = 'Dia';
      localStorage.setItem('tema', JSON.stringify(this.tema));
      localStorage.setItem('fondotexto', JSON.stringify(this.fondotexto));
    }    
  }
}


interface sour {
  name: string,
  url: string
}