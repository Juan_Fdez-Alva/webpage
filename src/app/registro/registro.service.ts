import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(private httpClient: HttpClient) { }

  registro(n, e, p, ma) {
    const credenciales = {nombre: n, email: e, password: p, mensageaviso: ma};
    return this.httpClient.post('./recursos/registro.php', credenciales);
  }
}
