import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { RegistroService } from './registro.service'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  constructor(private regService: RegistroService) { }

  registrado: boolean;

  mensageaviso = false;
  terminos = false;

  registradomsg = false;
  termaler = false;

  msgpsd = false;
  msgs = false;

  nombre = '';
  email = '';
  password = '';
  confimpassword = '';

  ngOnInit() {
    this.mensageaviso = false;
    this.terminos = false;
    this.termaler = false;
    this.msgs = false;
    this.nombre = '';
    this.email = '';
    this.password = '';
    this.confimpassword = '';
  }

  alerta() {
    if (this.terminos) {
      this.termaler = true;
      this.msgs = false;
    }
  }


  registro() {
    if (!this.terminos) {
      this.msgs = true;
      this.registradomsg = true;
    } else {
      if (!(this.password == this.confimpassword)) {
        this.msgpsd = true;
        this.registradomsg = true;
      } else {
        this.msgpsd = false;
        this.regService.registro(this.nombre, this.email, this.password, this.mensageaviso).toPromise().then(respuesta => {
          const response: any = respuesta;
          this.registrado = Boolean(response.regitrado);
          if (this.registrado) {
            console.log('registrado'); 
          }
          this.registradomsg = this.registrado;
        });

      }
    }
  }
}
