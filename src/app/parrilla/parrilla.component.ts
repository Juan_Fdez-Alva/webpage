import { Component, OnInit } from '@angular/core';
import { CommonService } from '../utils/common.service';

@Component({
  selector: 'app-parrilla',
  templateUrl: './parrilla.component.html',
  styleUrls: ['./parrilla.component.scss']
})
export class ParrillaComponent implements OnInit {

  constructor(private service: CommonService) { }

  parrilla = [];

  ngOnInit(): void {
    this.loadHoarios();
  }

  updateData() {
    this.parrilla = [];
    this.loadHoarios();
  }

  loadHoarios() {
    this.service.obtener('horarios').toPromise().then((data: Array<object>) => {
      data.forEach(elem => {
        this.parrilla.push(elem);
      });
    })
  }
}
