import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/loginpg/login.service';
import { Router } from '@angular/router';
import { SesionModel } from 'src/app/session.model';

@Component({
  selector: 'app-loginpg',
  templateUrl: './loginpg.component.html',
  styleUrls: ['./loginpg.component.scss']
})
export class LoginpgComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router) { }

  login = false;
  regis = false;
  cerrar = false;

  email = '';
  password = '';
  response: SesionModel;
  msgs = false;

  displayModal = false;

  entrar() {
    this.loginService.checkLogin(this.email, this.password).toPromise().then(respuesta => {
      const response: any = respuesta;
      response.accesos = Number(response.accesos);
      this.response = response;
      if (this.response.loggedin) {
        this.router.navigate(['menu']);
        sessionStorage.setItem('data', JSON.stringify(response));
      } else {
        this.msgs = true;
      }
    });
  }

  ngOnInit() {
    this.login = false;
    this.regis = false;
    this.cerrar = false;
  
    this.email = '';
    this.password = '';
    this.msgs = false;

    this.displayModal = false;
  }

}
