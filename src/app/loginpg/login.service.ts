import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  checkLogin(e, p) {
    const credenciales = { email: e, password: p };
    return this.httpClient.post('./recursos/login.php', credenciales);
  }
}
