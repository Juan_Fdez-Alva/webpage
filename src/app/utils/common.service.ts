import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RecursoModel } from './Recurso.model';
import { environment } from 'src/environments/environment';

import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient: HttpClient, private messageService: MessageService) { }

  getCurrentSong() {
    const fecha = new Date((new Date().getTime() + 3600000)).toJSON().slice(0, 10);
    return this.httpClient.get(environment.endpoint+'/recursos/nuevo.php?fecha=' + fecha);
  }

  obtener(recurso) {
    const recursofinal = new RecursoModel(recurso);
    const url = './recursos/';
    if (recurso == 'cancion') {
      const fecha = new Date((new Date().getTime() + 3600000)).toJSON().slice(0, 10);
      const consulta = {recurso:"cancion",fecha:fecha}
      return this.httpClient.post(environment.endpoint+'/inicio/recursos/index.php', consulta);
      // return this.httpClient.post(environment.endpoint+'/recursos/?recurso=cancion&fecha=' + fecha, {recurso:'cancion'});
    } else {
      return this.httpClient.get(url + '?recurso=' + recurso + '');
    }
  }

}
